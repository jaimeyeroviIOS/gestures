//
//  DeberViewController.swift
//  Gestures
//
//  Created by Jaime Yerovi on 20/1/18.
//  Copyright © 2018 jy. All rights reserved.
//

import UIKit

class DeberViewController: UIViewController {

    
    func alertGest(titlePress:String, messagePress:String){
        
        let alert = UIAlertView(title: titlePress, message: messagePress, delegate: self, cancelButtonTitle: "Entendido")
        alert.show()
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    
    @IBAction func buttonPress(_ sender: Any) {
        alertGest(titlePress: "Boton presionado 1 vez", messagePress: "Un solo tap")
        
    }
    
    
    @IBAction func longPressAction(_ sender: UILongPressGestureRecognizer) {
        alertGest(titlePress: "Tap Largo", messagePress: "En cuaquier parte de la pantalla")
    }
    
    
}
