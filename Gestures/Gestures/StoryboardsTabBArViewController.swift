//
//  StoryboardsTabBArViewController.swift
//  Gestures
//
//  Created by Jaime Yerovi on 23/1/18.
//  Copyright © 2018 jy. All rights reserved.
//

import UIKit

class StoryboardsTabBArViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }

 
    override func viewDidAppear(_ animated: Bool) {
        let left = tabBar.items![0]
        let right = tabBar.items![1]
        
        left.image = #imageLiteral(resourceName: "ic_picture_in_picture")
        right.image = #imageLiteral(resourceName: "ic_swap_horiz")
        
        left.title = "Tap"
        right.title = "Swipe"
        
        right.badgeColor = .red
        right.badgeValue = "6"
    }
    
}
