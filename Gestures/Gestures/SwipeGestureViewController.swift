//
//  SwipeGestureViewController.swift
//  Gestures
//
//  Created by Jaime Yerovi on 17/1/18.
//  Copyright © 2018 jy. All rights reserved.
//

import UIKit

class SwipeGestureViewController: UIViewController {

    
    @IBOutlet weak var customView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

       
    }

    
    @IBAction func swipeupAction(_ sender: Any) {
        
        customView.backgroundColor = .black
        
    }
    
    
    @IBAction func swipedownAction(_ sender: Any) {
         customView.backgroundColor = .red
    }
    
    
    @IBAction func swipeleftAction(_ sender: Any) {
         customView.backgroundColor = .yellow
    }
    
    @IBAction func swiperightAction(_ sender: Any) {
         customView.backgroundColor = .green
    }
    
    
}
