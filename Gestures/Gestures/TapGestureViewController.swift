//
//  TapGestureViewController.swift
//  Gestures
//
//  Created by Jaime Yerovi on 17/1/18.
//  Copyright © 2018 jy. All rights reserved.
//

import UIKit

class TapGestureViewController: UIViewController {

    @IBOutlet weak var touchesLabel: UILabel!
    
    @IBOutlet weak var tapLabel: UILabel!
    
    @IBOutlet weak var coordLabel: UILabel!
    
    @IBOutlet weak var customView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

     
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let touchCount = touches.count
        let tap = touches.first
        let tapCount = tap?.tapCount
        
        touchesLabel.text = "\(touchCount ?? 0)"
        tapLabel.text = "\(tapCount ?? 0)"
        
    }

    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let touch = touches.first
        
        let point = touch?.location(in: self.view)
        
        let x = point!.x
        let y = point!.y
        
        coordLabel.text = "x: \(x), y: \(y)"
        
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        print("Termino!")
        
    }
    
    @IBAction func tapGestureAction(_ sender: UITapGestureRecognizer) {
        
//        var taps = 0
//
//        if sender.state == .ended {
//            taps += 1
//            tapLabel.text = "\(taps)"
//        }
//
        customView.backgroundColor = .red
        
    }

    
}
